package ru.twitter.project.dao;

import ru.twitter.project.model.Log;

public interface LogDao extends AbstractDao<Log, Long> {
}
