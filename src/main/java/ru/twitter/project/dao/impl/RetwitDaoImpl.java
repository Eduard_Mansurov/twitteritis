package ru.twitter.project.dao.impl;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ru.twitter.project.dao.RetwitDao;
import ru.twitter.project.model.Retwit;
import ru.twitter.project.model.User;

import java.util.List;

@Repository
public class RetwitDaoImpl extends AbstractDaoImpl<Retwit, Long> implements RetwitDao {

    protected RetwitDaoImpl() {
        super(Retwit.class);
    }

    @Override
    public List<Retwit> findByUser(User user) {
        return (List<Retwit>) getCurrentSession().createCriteria(Retwit.class)
                .add(Restrictions.eq("user", user))
                .addOrder(Order.desc("date")).list();
    }

    @Override
    public List<Retwit> findByTwit(Long id) {
        return (List<Retwit>) getCurrentSession().createCriteria(Retwit.class)
                .add(Restrictions.eq("twit.id", id)).list();
    }
}
