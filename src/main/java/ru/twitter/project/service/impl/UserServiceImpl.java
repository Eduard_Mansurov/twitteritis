package ru.twitter.project.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.twitter.project.aop.Loggable;
import ru.twitter.project.dao.UserDao;
import ru.twitter.project.form.UserForm;
import ru.twitter.project.model.User;
import ru.twitter.project.service.UserService;
import ru.twitter.project.utils.FormMapper;
import ru.twitter.project.utils.MD5Generator;

import java.util.List;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    @Loggable
    public User findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    @Loggable
    public User addUser(UserForm userForm) {
        User user = FormMapper.userFormToUser(userForm);
        String password = user.getPassword();
        user.setPassword(MD5Generator.md5(password));
        userDao.save(user);
        return user;
    }

    @Override
    @Loggable
    public List<User> findAll() {
        return userDao.findAll();
    }
}
