<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<t:template title="home">
<jsp:attribute name="body">
        <form:form modelAttribute='twit' commandName="twit" action="/page/newTwit" method='POST'>
        <div class="container-fluid" id="login_logout">

        <div class="shadow-z-1 well-material-green-A700" style="height: 19vh">
            <security:authorize access="isAuthenticated()">
                <h4> User: <security:authentication property="principal.name"/></h4>
                <a href="/logout" class="addButton"> Logout </a>
            </security:authorize>
            <security:authorize access="isAnonymous()">
                <h4> User: <security:authentication property="principal"/></h4>
                <a href="/login" class="addButton"> Login </a>
            </security:authorize>
        </div>

        <div class="container top horizontal-center">
        <c:if test="${user.id == myUser}">
            <div class="shadow-z-2 topArea" style="background: white">

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <form:textarea type="text" path="twitText"
                                               class="form-control floating-label"
                                               rows="3" placeholder="New twit"
                                               id="twitInput"></form:textarea>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <form:textarea type="text" path="location"
                                               class="form-control floating-label"
                                               rows="3" placeholder="Location"
                                               id="locationInput"></form:textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="errorText" class="text-danger col-lg-2"
                             style="margin-left: 68%; margin-top: 1%"></div>
                        <div class="teste">
                            <form:button path="userID" value="${user.id}" type="submit"
                                         class="btn btn-primary btn-raised"
                                         id="submitButton">submit</form:button>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="shadow-z-2 fill" style="background: white">
            <div>
                <div class="teste1" style="padding-top: 7%; padding-bottom: 1%">

                    <c:forEach var="entry" items="${list}">
                        <div class="twit${entry.id}" style="margin-bottom: 1%">
                            <div>
                                <c:if test="${entry.getClass().simpleName == 'Twit'}"><strong>${entry.user.name}</strong></c:if>

                                <div style="float: right" class="text-muted">
                                    <c:if test="${(entry.getClass().simpleName == 'Twit') && (entry.location != null) && (entry.location != '')}"><label
                                            class="twitLocation">${entry.location}</label>,</c:if>
                                    <c:if test="${(entry.getClass().simpleName == 'Twit')}">
                                        <label class="date">
                                                <%--<script type="text/javascript">document.write(moment(<c:out value="${entry.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));</script>--%>
                                            <script type="text/javascript">document.write(moment(<c:out value="${entry.date}"/>).endOf('weeek').fromNow());</script>
                                        </label>
                                    </c:if>
                                </div>
                            </div>


                            <c:if test="${(entry.getClass().simpleName == 'Retwit') && (entry.flag)}">
                                <div class="text-muted">${entry.user.name} retweeted
                                    <div style="float: right">
                                        <script type="text/javascript">document.write(moment(<c:out value="${entry.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));</script>
                                    </div>
                                </div>
                            </c:if>


                            <c:if test="${(entry.getClass().simpleName == 'Retwit')  && (!entry.flag)}">
                                <div class="text-muted">
                                    @${entry.twit.user.name} <a
                                        href="/page/${entry.twit.user.name}#${entry.twit.id}">Some text</a>

                                    <div style="float: right" class="text-muted">
                                        <c:if test="${(entry.getClass().simpleName == 'Retwit') && !(entry.flag)}">
                                            <script type="text/javascript">document.write(moment(<c:out value="${entry.twit.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));</script>
                                        </c:if>
                                    </div>
                                </div>

                            </c:if>
                            <div>
                                <c:if test="${(entry.getClass().simpleName == 'Retwit') && (entry.flag)}"><strong>${entry.twit.user.name}</strong></c:if>
                                <div style="float: right" class="text-muted">
                                    <c:if test="${(entry.getClass().simpleName == 'Retwit') && (entry.twit.location != null) && (entry.twit.location != '')}">${entry.twit.location},</c:if>
                                    <c:if test="${(entry.getClass().simpleName == 'Retwit') && (entry.flag)}">
                                        <script type="text/javascript">document.write(moment(<c:out value="${entry.twit.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));</script>
                                    </c:if>

                                </div>

                            </div>

                            <div>
                                <c:if test="${(entry.getClass().simpleName == 'Retwit') && !(entry.flag)}"><strong>${entry.user.name}</strong></c:if>
                                <div style="float: right" class="text-muted">
                                    <c:if test="${(entry.getClass().simpleName == 'Retwit') && !(entry.flag)}">
                                        <script type="text/javascript">document.write(moment(<c:out value="${entry.date}"/>).format('MMMM Do YYYY, h:mm:ss a'));</script>
                                    </c:if>
                                </div>

                            </div>
                            <div>
                                <c:if test="${entry.getClass().simpleName == 'Twit'}">
                                    <h4><p class="textTwit"><a name="${entry.id}"></a>${entry.text}</p></h4>
                                </c:if>
                                <c:if test="${(entry.getClass().simpleName == 'Retwit') && !(entry.flag)}">
                                    <h4><p> ${entry.answerText}
                                    </p></h4>
                                </c:if>
                                <c:if test="${(entry.getClass().simpleName == 'Retwit') && (entry.flag)}">
                                    <h4><p>${entry.twit.text}</p></h4>
                                </c:if>
                            </div>
                            <div class="row">
                                <c:if test="${entry.user.id == myUser}">
                                    <div class="icon-preview" style="float: right">
                                        <i class="mdi-action-delete deleteButton ${entry.getClass().simpleName}"
                                           twitid="${entry.id}"><span>delete</span></i>
                                    </div>
                                    <c:if test="${entry.getClass().simpleName == 'Twit'}">
                                        <div class="icon-preview" style="float: right">
                                            <i class="mdi-editor-mode-edit editButton"
                                               twitid="${entry.id}"><span>edit</span></i>
                                        </div>
                                    </c:if>
                                </c:if>
                                <c:if test="${entry.user.id != myUser}">
                                    <div class="icon-preview" style="float: right">
                                        <i class="mdi-av-repeat retwitButton"
                                           twitid="${entry.id}"><span>retwit</span></i>
                                    </div>
                                </c:if>
                                <div class="icon-preview" style="float: right">
                                    <i class="mdi-content-reply replyButton"
                                       data-toggle="modal" data-target="#complete-dialog"
                                       twitid="${entry.id}"><span>reply</span></i>

                                    <div id="complete-dialog" class="modal fade" tabindex="-1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal" aria-hidden="true">×
                                                    </button>
                                                    <h4 class="modal-title">Dialog</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><form:textarea type="text" path="twitText"
                                                                      class="form-control floating-label"
                                                                      placeholder="New twit"
                                                                      id="answerInput"></form:textarea></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary answerButton"
                                                            data-dismiss="modal">Answer
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <c:if test="${entry.user.id == myUser}">
                                    <div class="privateAccess icon-preview" style="float: right">
                                        <i class="mdi-social-people privateButton"
                                           data-toggle="modal" data-target="#access${entry.id}"
                                           twitid="${entry.id}"
                                           <c:if test="${entry.access != 'private'}">style="display: none"</c:if>><span>Edit access</span></i>
                                    </div>
                                    <div class="private"
                                         messageType="<c:out value="${entry.getClass().simpleName == 'Twit' ? 'twit': 'retwit'}"/>"
                                         twitId="${entry.id}" style="float: right">
                                        <select class="form-control select" placeholder="Кому виден твит?">
                                            <option
                                                    <c:if test="${entry.access == 'open'}">selected</c:if>
                                                    value="open">Открытый твит</option>
                                            <option
                                                    <c:if test="${entry.access == 'private'}">selected</c:if>
                                                    value="private">Приватный твит</option>
                                            <option
                                                    <c:if test="${entry.access == 'close'}">selected</c:if>
                                                    value="close">Закрытый от всех</option>
                                        </select>
                                    </div>
                                    <div id="access${entry.id}" class="modal fade"
                                         tabindex="-1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal" aria-hidden="true">
                                                        ×
                                                    </button>
                                                    <h4 class="modal-title">Dialog</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <%--<c:forEach var="user" items="${entry.accessUsers}">--%>

                                                        <%--</c:forEach>--%>

                                                    <form:textarea type="text" path="access"
                                                                   class="form-control floating-label newAccess${entry.id}"
                                                                   rows="3" placeholder="New access"
                                                            ></form:textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary editAccess"
                                                            twitId="${entry.id}" data-dismiss="modal"
                                                            messageType="<c:out value="${entry.getClass().simpleName == 'Twit' ? 'twit': 'retwit'}"/>">
                                                        EDIT
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        </div>
        </div>
        </form:form>

</jsp:attribute>
    <jsp:attribute name="head">
    </jsp:attribute>
</t:template>