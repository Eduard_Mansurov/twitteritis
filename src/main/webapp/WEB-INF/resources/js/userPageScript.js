;
$(document).ready(function () {
    var csrf = $('input[name=_csrf]').val();

    $(".select").dropdown({ "autoinit": ".select" });

    $('body').on('click', '.dropdown input', function () {
        $('li[value=private]').attr("data-toggle", "modal").attr("data-target", "#complete-dialog"
            + $(this).parent('private').attr('twitId'));
    });


    //TODO optimize
    $('body').on('click', 'li', function () {
        var parent = $(this).parent().parent().parent();
        var id = $(parent).attr('twitid');
        var url;
        var access = $(this).attr('value');
        if ($(parent).attr('messagetype') == 'twit') {
            url = "/changeTwitAccess";
        } else {
            url = "/changeRetwitAccess";
        }
        if (access == "private") {
            $('.privateButton[twitid=' + id + ']').attr('style', 'display: inline-block;');
        } else {
            $('.privateButton[twitid=' + id + ']').attr('style', 'display: none;');
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {"id": id, "access": access, "_csrf": csrf},
            success: function () {
            },
            error: function (data) {
                console.log(data);
            }

        });
    });

//    $(function() {
//        vpw = $(window).width();
//        vph = $(window).height();
//        $('.container').css({'left': (vpw - $('.container').width())/2});
//    });

    $('body').on('click', '#submitButton', function (e) {
        e.preventDefault();
        var text = $('#twitInput').val();
        var location = $('#locationInput').val();
        var id = $(this).val();
        var access = "open";
        if (text != null && text != undefined && text != ''
            && text.replace(/\s{2,}/g, ' ') != ' ') {
            $.ajax({
                type: "POST",
                url: "/page/newTwit",
                data: {"userID": id, "location": location, "twitText": text, "access": access, "_csrf": csrf},
                success: function (data) {
                    $('.teste1').prepend(data);
                    $('#twitInput').val('').addClass('empty');
                    $('#locationInput').val('').addClass('empty');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        } else {
            $('#errorText').html("<h4><strong>Twit is empty!</strong></h4>");
        }
    });

    $('body').on('click', '.deleteButton', function () {
        var id = $(this).attr('twitid') + "";
        var url;
        if ($(this).hasClass('Twit')) {
            url = "/deleteTwit";
        } else {
            url = "/deleteRetwit";
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {"id": id, "_csrf": csrf},
            success: function () {
                $('.twit' + id).remove();
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $('.retwitButton').on('click', function () {
        var id = $(this).attr('twitid');
        var flag = true;
        var  access = "open";
        $.ajax({
            type: "POST",
            url: "/page/newRetwit",
            data: {"twitId": id, "RetwitOrAnswer": flag, "access": access , "_csrf": csrf},
            success: function (data) {
                //TODO OK message
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $('.replyButton').on('click', function () {
        $('.answerButton').attr('value', $(this).attr('twitid'));
    });

    $('body').on('click', '.answerButton', function () {
        var text = $('#answerInput').val();
        var id = $(this).val();
        var flag = false;
        var  access = "open";
        $.ajax({
            type: "POST",
            url: "/page/newRetwit",
            data: {"twitId": id, "answerText": text, "access": access, "RetwitOrAnswer": flag, "_csrf": csrf},
            success: function (data) {
                //TODO OK message
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $('body').on('click', '.editButton', function () {
        var id = $(this).attr('twitid') + "";
        var textTwit = $('.twit' + id + ' .textTwit').text();
        var locationTwit = $('.twit' + id + ' .twitLocation').text();
        var classPrev = $('#submitButton').attr('class');
        $('#twitInput').val(textTwit).removeClass('empty');
        $('#locationInput').val(locationTwit);
        if (locationTwit != '' && locationTwit != null) {
            $('#locationInput').removeClass('empty');
        }
        $('#submitButton').attr('id', 'editSubmit');
        $('#editSubmit').attr('class', classPrev + " " + id);
        $('#editSubmit').text('edit');
    });

    $('body').on('click', '#editSubmit', function (e) {
        e.preventDefault();
        var textNEW = $('#twitInput').val();
        var locationNew = $('#locationInput').val();
        var twitID = $(this).attr('class').substring(27);
        var id = $('#editSubmit').val();
        var datestr = $('.twit' + twitID + ' .date').text().substring(75, 87);
        var classPrev = "btn btn-primary btn-raised";
        var access = $('.twit' + twitID + ' li[selected=selected]').val();
        $.ajax({
            type: "POST",
            url: "/page/editTwit",
            data: {"id": twitID, "userID": id, "date": datestr, "location": locationNew,
                "twitText": textNEW, "access": access, "_csrf": csrf},
            success: function () {
                $('.twit' + twitID + ' .twitLocation').html(locationNew);
                $('.twit' + twitID + ' .textTwit').html(textNEW);
                $('#editSubmit').text('submit').attr('id', 'submitButton');
                $('#submitButton').attr('class', classPrev);
                $('#twitInput').val('').addClass('empty');
                $('#locationInput').val('').addClass('empty');
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    //script fot access
    $('body').on('click', '.editAccess', function() {
        var idTwit = $(this).attr('twitid');
        var setOfUsers = $('.newAccess' + idTwit).val();
        var type = $(this).attr('messageType');
        $.ajax({
            type: "POST",
            url: "/accessEdit",
            data: {"id": idTwit, "list": setOfUsers, "type": type, "_csrf": csrf},
            success: function() {

            },
            error: function() {
                console.log();
            }

        });
    });

});

