<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@tag description="Simple Template" pageEncoding="UTF-8" %>

<%@attribute name="title" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="body" fragment="true" required="true" %>

<html>
<head>
    <title>${title}</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/roboto.min.css" rel="stylesheet">
    <link href="/resources/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/resources/css/ripples.min.css" rel="stylesheet">
    <link href="/resources/css/jquery.dropdown.css" rel="stylesheet">
    <link href="/resources/css/userPageStyle.css" rel="stylesheet">

    <%--<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>--%>
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
    <script type="text/javascript" src="/resources/js/ripples.min.js"></script>
    <script type="text/javascript" src="/resources/js/material.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/userPageScript.js"></script>
    <script type="text/javascript" src="/resources/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="/resources/js/jquery.dropdown.js"></script>
    <jsp:invoke fragment="head"/>
</head>
<body>
<script type="text/javascript">$(function () {
    $.material.init();
});</script>
    <jsp:invoke fragment="body"/>
</body>
</html>

